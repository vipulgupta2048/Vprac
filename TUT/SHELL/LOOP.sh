#!/bin/sh
#This shell script will be about FOR loops

#for i in 1 2 3 4 5
#do
	#echo "THIS IS BEING LOOPED $i many times"
#done


for i in hello 1 * 2 goodbye
do
	echo "Looping $i many times"
done

#Here star (*) takes everything that it is in the directory and loops over it.
